import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, ToastController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ListEventosPage } from '../pages/list-eventos/list-eventos';
import { ListEventosCriadosPage } from '../pages/list-eventos-criados/list-eventos-criados';
// import { LoginPage } from '../pages/login/login';
import { AuthService } from '../providers/auth-service/auth-service';
import { AngularFireAuth } from 'angularfire2/auth';
import { EditarUsuarioPage } from "../pages/editar-usuario/editar-usuario";
import { HomePage } from '../pages/home/home';
import { IngressosCompradosPage } from '../pages/ingressos-comprados/ingressos-comprados';
import { UsuarioService } from '../providers/usuario-service/usuario-service';
import firebase from 'firebase';
import { Subject } from 'rxjs/Subject';


@Component({
  selector: 'app-root',
  templateUrl: 'app.html'
})
export class MyApp {

  @ViewChild(Nav) nav: Nav;
  // rootPage: any = LoginPage;
  rootPage: any = HomePage;
  activePage
  pages: Array<{ title: string, component: any, icon: string, show: boolean }>;
  pageSubject = new Subject();
  showMeusEventos: any;
  id: any;
  usuario: any;

  constructor(public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    private authService: AuthService,
    public toastCtrl: ToastController,
    private afAuth: AngularFireAuth,
    private usuarioService: UsuarioService) {
    this.initializeApp();


    this.pages = [
      { title: 'Eventos', component: ListEventosPage, icon: 'map', show: true },
      { title: 'Meus Eventos', component: ListEventosCriadosPage, icon: 'paper', show: true },
      { title: 'Editar Usuário', component: EditarUsuarioPage, icon: 'person', show: true },
      { title: 'Meus Ingressos', component: IngressosCompradosPage, icon: 'barcode', show: true },
      { title: 'Sair', component: HomePage, icon: 'exit', show: true }
    ];
    this.pageSubject.subscribe((selectedPage: any) => {
      this.afAuth.authState.subscribe((usuario: any) => {
        if (usuario) {
          this.usuarioService.buscarPorId(usuario.uid).subscribe((usuarioAux: any) => {
            this.usuario = usuarioAux;
            this.pages.map(page => {
              page.show = !(page.component == ListEventosCriadosPage && (this.usuario.tipoUsuario == undefined || this.usuario.tipoUsuario == 0));
              return page;
            });
          });
        }
      });
    });
    
    const authSubscribe = this.afAuth.authState.subscribe((usuario: any) => {
      authSubscribe.unsubscribe();     
      if(usuario){
        this.nav.setRoot(ListEventosPage);
       } else {
        this.nav.setRoot(this.pages[4].component);
        this.pageSubject.next(this.pages[4]);
       }
    });
    
  }

  atualizarMenu() {
    const authSubscribe = this.afAuth.authState.subscribe((usuario: any) => {
      authSubscribe.unsubscribe();
      const usuarioSubscribe = this.usuarioService.buscarPorId(usuario.uid).subscribe((usuarioAux: any) => {
        usuarioSubscribe.unsubscribe();
        this.usuario = usuarioAux;
        // if (usuarioAux.tipoUsuario == undefined)
        //   this.showMeusEventos = 0;
        // else
        //   this.showMeusEventos = usuarioAux.tipoUsuario;
      });
      console.log('logado', usuario)
      this.openPage(this.pages[0]);
      this.pageSubject.next(this.pages[0]);
    });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page: any) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    let toast = this.toastCtrl.create({ duration: 3000, position: 'bottom' });
    if (page.component == HomePage) {
      let user = firebase.auth().currentUser;
      if (user) {
        this.authService.signOut().catch((error: any) => {
          console.error(error)
        });
      }
    } else {
      this.activePage = page;
      this.pageSubject.next(page);
    }    
    this.nav.setRoot(page.component);
  }

  checkActive(page) {
    return page == this.activePage;
  }

  menuOpened() {
    // this.atualizarMenu();
  }

}
