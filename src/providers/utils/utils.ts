import { Injectable } from '@angular/core';

/*
  Generated class for the UtilsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UtilsProvider {

  private n: any;
  private len: any;

  detectAmount(v): string {
    if (v) {
      this.n = v[v.length - 1];
      if (isNaN(this.n)) {
        v = v.substring(0, v.length - 1);
        return v;
      }
      v = this.fixAmount(v);
      return v;
    }
  }

  private fixAmount(a): string {
    let period = a.indexOf(".");
    if (period > -1) {
      a = a.substring(0, period) + a.substring(period + 1);
    }
    this.len = a.length;
    while (this.len < 3) {
      a = "0" + a;
      this.len = a.length;
    }
    a = a.substring(0, this.len - 2) + "." + a.substring(this.len - 2, this.len);
    while (a.length > 4 && (a[0] == '0')) {
      a = a.substring(1)
    }
    if (a[0] == ".") {
      a = "0" + a;
    }
    return (a);
  }

  descClassificacao(opcao){
    if(opcao == '0')
      return 'N/A';
    else if(opcao == '1')
      return 'Livre';
    else if (opcao == '2')
      return 'A partir de 10 anos';
    else if (opcao == '3')
      return 'A partir de 12 anos';
    else if (opcao == '4')
      return 'A partir de 14 anos';
    else if (opcao == '5')
      return 'A partir de 16 anos';
    else if (opcao == '6')
      return 'A partir de 18 anos';
    else 
      return 'N/A'
  }

  validarHoraFinal(horaFinal) {
    return (horaFinal !== undefined && horaFinal !== null && horaFinal !== ''  ? ' até ' + horaFinal + 'h' : '')
  }

  infoDataHora(evento){
    return 'A partir de ' +  evento.horaInicial + 'h ' + this.validarHoraFinal(evento.horaFinal);
  }

  constructor() {
  }

}
