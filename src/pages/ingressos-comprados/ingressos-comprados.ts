import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { IngressoServiceProvider } from '../../providers/ingresso-service/ingresso-service';
import { UtilsProvider } from '../../providers/utils/utils';
import { UsuarioService } from '../../providers/usuario-service/usuario-service';

/**
 * Generated class for the IngressosCompradosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-ingressos-comprados',
  templateUrl: 'ingressos-comprados.html',
})
export class IngressosCompradosPage {

  ingressos:any;
  
  ingressoSubject;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              private ingressoService: IngressoServiceProvider,
              private utils: UtilsProvider,
              private usuarioService: UsuarioService) {
      this.ingressos = this.ingressoService.consultarIngressosAndEventoPorUsuario(usuarioService.getUsuarioKey());
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad IngressosCompradosPage');
  }

  amountChange(valorIngresso) {
    if (valorIngresso)
      return 'R$' + this.utils.detectAmount(valorIngresso);
    else
      return 'Gratuito'
  }

  infoDataHora(evento){
    return this.utils.infoDataHora(evento);
  }

}
