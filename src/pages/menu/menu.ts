import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, ToastController, IonicPage } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ListEventosPage } from '../list-eventos/list-eventos';
import { ListEventosCriadosPage } from '../list-eventos-criados/list-eventos-criados';
// import { LoginPage } from '../pages/login/login';
import { AngularFireAuth } from 'angularfire2/auth';
import { EditarUsuarioPage } from "../editar-usuario/editar-usuario";
import { HomePage } from '../home/home';
import { IngressosCompradosPage } from '../ingressos-comprados/ingressos-comprados';
import { UsuarioService } from '../../providers/usuario-service/usuario-service';
import firebase from 'firebase';
import { AuthService } from '../../providers/auth-service/auth-service';

/**
 * Generated class for the MenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',
})
export class MenuPage {
  @ViewChild(Nav) nav: Nav;
  // rootPage: any = LoginPage;
  rootPage: any = HomePage;
  pages: Array<{ title: string, component: any, icon: string, show: boolean }>;
  activePage: any;
  showMeusEventos: any;
  id: any;
  usuario: any;

  constructor(public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    private authService: AuthService,
    public toastCtrl: ToastController,
    private afAuth: AngularFireAuth,
    private usuarioService: UsuarioService) {


    this.pages = [
      { title: 'Eventos', component: ListEventosPage, icon: 'map', show: true },
      { title: 'Meus Eventos', component: ListEventosCriadosPage, icon: 'paper', show: true },
      { title: 'Editar Usuário', component: EditarUsuarioPage, icon: 'person', show: true },
      { title: 'Meus Ingressos', component: IngressosCompradosPage, icon: 'barcode', show: true },
      { title: 'Sair', component: HomePage, icon: 'exit', show: true }
    ];

    // this.atualizarMenu();
  }

  ionViewCanEnter() {
    this.atualizarMenu();
  }

  ionViewWillLoad() {
    this.atualizarMenu();
  }

  ionViewWillEnter() {
    this.atualizarMenu();
  }

  atualizarMenu() {
    this.afAuth.authState.subscribe((usuario: any) => {
      this.usuarioService.buscarPorId(usuario.uid).subscribe((usuarioAux: any) => {
        this.usuario = usuarioAux;
        // if (usuarioAux.tipoUsuario == undefined)
        //   this.showMeusEventos = 0;
        // else
        //   this.showMeusEventos = usuarioAux.tipoUsuario;
      })
      console.log('logado', usuario)
      this.openPage(this.pages[0]);
    });
  }

  openPage(page: any) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    let toast = this.toastCtrl.create({ duration: 3000, position: 'bottom' });
    if (page.component == HomePage) {
      let user = firebase.auth().currentUser;
      if (user) {
        this.authService.signOut().catch((error: any) => {
          console.error(error)
        });
      }
    } else {
      if (page.component == ListEventosCriadosPage && (this.usuario.tipoUsuario == undefined || this.usuario.tipoUsuario == 0)) {
        toast.setMessage('É necessario ser um Divugador para Criar Eventos.');
        toast.present();
        return;
      } else {
        this.activePage = page;
      }
    }

    this.nav.setRoot(page.component);
  }

  checkActive(page) {
    return page == this.activePage;
  }

  menuOpened() {
    // this.atualizarMenu();
  }

}
