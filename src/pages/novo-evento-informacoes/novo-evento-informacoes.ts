import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController, LoadingController } from 'ionic-angular';
import { EventoService } from '../../providers/evento-service/evento-service';
import { ListEventosCriadosPage } from '../list-eventos-criados/list-eventos-criados';
import firebase from 'firebase';
import { UtilsProvider } from '../../providers/utils/utils';
import { FormBuilder, Validators, FormControl } from '@angular/forms';

/**
 * Generated class for the NovoEventoInformacoesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-novo-evento-informacoes',
  templateUrl: 'novo-evento-informacoes.html',
})
export class NovoEventoInformacoesPage {

  formGroup;
  public mask = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]
  NOME_PARTERN = /^[A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ\s]+$/;
  evento: any;
  tituloPagina: string;
  filePhoto: File;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    private eventoService: EventoService,
    private utils: UtilsProvider,
    private loadCtrl: LoadingController) {


    this.evento = this.navParams.data.evento || {'classificacao': 0};
    this.setarTituloPagina();
  }

  setarTituloPagina() {
    if (this.evento.key) {
      this.tituloPagina = 'Alterar informações do evento'
    } else {
      this.tituloPagina = 'Informações do evento'
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NovoEventoInformacoesPage');
  }

  openPage() {
    let toast = this.toastCtrl.create({ duration: 3000, position: 'bottom' });

    if(this.evento.sinopse === undefined || this.evento.sinopse === null || this.evento.sinopse === ''){
      this.evento.sinopse = 'N/A';
    }

    if (this.evento.nome === undefined || this.evento.nome === null || this.evento.nome === '') {
      toast.setMessage('Nome é Obrigatório');
      toast.present();
      return;
    } else if (this.evento.data === undefined || this.evento.data === null || this.evento.data === '') {
      toast.setMessage('Data é Obrigatória');
      toast.present();
      return;
    } else if (this.evento.horaInicial === undefined || this.evento.horaInicial === null || this.evento.horaInicial === '') {
      toast.setMessage('Hora Inicial é Obrigatória');
      toast.present();
      return;
    } 

    let alert = this.alertCtrl.create({
      title: 'Confirmação de informações do evento',
      message: 'Confirma as informações: Nome do evento: ' + this.evento.nome +
        ', sinopse: ' + this.evento.sinopse + ', classificação: ' + this.utils.descClassificacao(this.evento.classificacao) +
        ', data: ' + this.evento.data + ', duração: ' + this.utils.infoDataHora(this.evento) ,
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Confirmar',
          handler: () => {

            if (this.filePhoto != null) {
              this.confirmarEventoComFoto(toast);
            } else {
              this.confirmarEventoSemFoto(toast);
            }
          }
        }
      ]
    });
    alert.present();
  }

  confirmarEventoComFoto(toast) {

    let loadAux = this.loadCtrl.create({
      spinner: 'bubbles',
      content: 'Aguarde um momento!'
    })
    let uploadTask = this.eventoService.uploadESalvar(this.filePhoto);
    uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
      (snapshot) => {
        loadAux.present();
        var progress = (uploadTask.snapshot.bytesTransferred / uploadTask.snapshot.totalBytes) * 100;
      },
      (error) => {
        console.error(error);
      }, () => {
        debugger;
        this.evento.fullPath = this.eventoService.getBasePath() + this.evento.nome + this.filePhoto;
        this.eventoService.getUploadRef(uploadTask.snapshot.metadata.fullPath).getDownloadURL().then((url) => {
          
          this.evento.url = url;

          if (this.evento.key) {
            this.eventoService.atualizar(this.evento).then(() => {
              toast.setMessage('Evento alterado com sucesso.');
              this.navCtrl.setRoot(ListEventosCriadosPage);
            }, (error) => {
              toast.setMessage('Erro ao alterar evento.');
            });
          } else {
            this.eventoService.salvar(this.evento).then(() => {
              toast.setMessage('Evento cadastrado com sucesso.');
              this.navCtrl.setRoot(ListEventosCriadosPage);
            }, (error) => {
              toast.setMessage('Erro ao cadastrar evento.');
            });
          }
        })
        loadAux.dismiss();
        toast.present();
      });

  }

  confirmarEventoSemFoto(toast) {

    this.evento.url = 'https://firebasestorage.googleapis.com/v0/b/projeto-localizador-espetaculo.appspot.com/o/semImage.png?alt=media&token=fe7847c9-c517-4f8d-aaf5-5296878cf9a2';
    if (this.evento.key) {
      this.eventoService.atualizar(this.evento).then(() => {
        toast.setMessage('Evento alterado com sucesso.');

        this.navCtrl.setRoot(ListEventosCriadosPage);
      }, (error) => {
        toast.setMessage('Erro ao alterar evento.');
      });
    } else {
      this.eventoService.salvar(this.evento).then(() => {
        toast.setMessage('Evento cadastrado com sucesso.');
        this.navCtrl.setRoot(ListEventosCriadosPage);
      }, (error) => {
        toast.setMessage('Erro ao cadastrar evento.');
      });
    }
    toast.present();
  }

  onPhoto(event): void {
    this.filePhoto = event.target.files[0];
  }

  amountChange() {
    this.evento.valorIngresso = this.utils.detectAmount(this.evento.valorIngresso);
  }

  

}
